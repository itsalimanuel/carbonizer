import React, { Component } from "react";
import "./index.scss";

class LinksPage extends Component {
  render() {
    return (
      <div className="">
        <h1>Links</h1>
        <a href=""> Link from carbon</a>
        <hr />
        <a href="">Link from carbon</a> <hr />
        <a href="" line="under">
          hover Link from carbon with line
        </a>
        <hr />
        <a href="" className="active">
          Active Link from carbon with line
        </a>
        <hr />
        <a href="" className="active focus">
          Active , Focus Link from carbon with line
        </a>
        <hr />
        <a href="" className="downlaod-link">
          Downlaod Link from carbon with line
        </a>
      </div>
    );
  }
}

export default LinksPage;
