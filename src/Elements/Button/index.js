import React, { Component } from "react";
import "./index.scss";

import image from "./img/logo.svg";

class Button extends Component {
  render() {
    return (
      <div className="">
        <h1>Buttons List</h1>
        <hr />
        <p>Buttons just icon</p>
        <span>btn</span>
        <button className="btn ">
          <div className="btn-icon">
            <img src={image} alt="" />
          </div>
        </button>
        <span> is-large</span>
        <button className="btn is-large">
          <div className="btn-icon">
            <img src={image} alt="" />
          </div>
        </button>
        <hr />
        <p>Buttons just text</p>
        <button className="btn ">click me</button>
        <hr />
        <p>Buttons text with icon</p>
        <button className="btn ">
          click me{" "}
          <div className="btn-icon wt">
            <img src={image} alt="" />
          </div>
        </button>
        <hr />
        <p>button without background</p>
        <button className="btn wb">
          <div className="btn-icon ">
            <img src={image} alt="" />
          </div>
        </button>
        <hr />
        <p>button for all brakepoints</p>

        <button className="btn" btn="1920" mt="10">
          btn="1920"
        </button>
        <button className="btn" btn="1600" mt="10">
          btn="1600"
        </button>
        <button className="btn" btn="1300" mt="10">
          btn="1300"
        </button>
        <button className="btn" btn="1024" mt="10">
          btn="1024"
        </button>
        <button className="btn" btn="768" mt="10">
          btn="768"
        </button>
        <button className="btn" btn="mobile" mt="10">
          btn="mobile"
        </button>
      </div>
    );
  }
}

export default Button;
