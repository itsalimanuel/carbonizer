import React, { Component } from "react";
import "./index.scss";

class Pagination extends Component {
  render() {
    return (
      <div id="Paginationpage">
        <h1>Pagination</h1>
        <hr />
        <div className="container justify-content-center md-justify-content-start">
          <ul className="pagination ">
            <li className="page-item">
              <a href="" className="page-link">
                Previous
              </a>
            </li>
            <li className="page-item">
              <a href="" className="page-link">
                1
              </a>
            </li>
            <li className="page-item">
              <a href="" className="page-link">
                2
              </a>
            </li>
            <li className="page-item">
              <a href="" className="page-link">
                3
              </a>
            </li>
            <li className="page-item">
              <a href="" className="page-link">
                Next
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default Pagination;
