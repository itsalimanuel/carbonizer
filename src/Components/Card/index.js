import React, { Component } from "react";
import image from "./img/image.jpg";
import "./index.scss";

class Card extends Component {
  render() {
    return (
      <div id="cardPublic">
        <h1>Card System</h1>
        <div id="card">
          <div className="card" card>
            <div className="card-image">
              <figure className="h300">
                <img src={image} alt="" />
              </figure>
            </div>
            <div className="card-content">
              <div className="media">
                <div className="media-icon">
                  <figure>
                    <img src={image} alt="" />
                  </figure>
                </div>
                <div className="media-content" width="100">
                  <h1>Card Title</h1>
                </div>
              </div>
              <div className="content">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Sapiente numquam aliquam <a href="">blanditiis</a> perspiciatis
                accusamus vero voluptate quod non laudantium repellat, ut, iste
                fuga tempore consequatur cum quia odit libero. Consectetur.
              </div>
            </div>
            <div className="card-footer" width="100">
              <a href="#Save" className="card-footer-item">
                Save
              </a>
              <a href="#edit" className="card-footer-item">
                Edit
              </a>
              <a href="#Delete" className="card-footer-item">
                Delete
              </a>
            </div>
          </div>
          <div className="El-description">
            <h1>
              The card component comprises several elements that you can mix and
              match:
            </h1>
            <ul>
              <div per="code">code</div> : the main container
              <li>
                <div per="code">card-image</div> : a fullwidth container for a
                responsive image
              </li>
              <li>
                <div per="code">card-content</div> : a multi-purpose container
                for any other element
                <ul marginLeft="20">
                  <li>
                    <div per="code">media</div> : a multi-purpose container for
                    any other element
                  </li>
                  <li>
                    <div per="code">media-icon</div> : a multi-purpose container
                    for any other element
                  </li>
                  <li>
                    <div per="code">media-content</div> : a multi-purpose
                    container for any other element
                  </li>
                </ul>
              </li>
              <li>
                <div per="code">card-header</div> : a horizontal bar with a
                shadow
              </li>
              <li>
                <div per="code">card-footer</div> : a horizontal bar with a
                shadow
                <ul marginLeft="20">
                  <li>
                    <div per="code">card-footer-item</div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div id="card2">
          <br />
          <h1>Card modal 2</h1>
          <div className="container">
            <div className="card" height="600">
              <div className="card-background">
                <img src={image} alt="" object="cover" />
              </div>
              <div class="card-content" is="bottom">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Assumenda iste inventore officiis dolores exercitationem
                reprehenderit pariatur, cum harum vitae similique ea repellat,
                recusandae tenetur dolorem corporis consequuntur obcaecati
                voluptatibus modi.
              </div>
            </div>
          </div>
        </div>
        <div id="card3" marginTop="30" display="flex">
          <div col="3">
            <div className="card">
              <div className="card-column">
                <div className="card-column-icon">
                  <img src={image} alt="" />
                </div>
                <div className="card-column-title">
                  <h1 fs="24">Pure Android No clutter</h1>
                </div>
                <div className="card-column-desc">
                  No manufacturer apps to slow your device Optimized for speed
                  and performance.
                </div>
              </div>
            </div>
          </div>
          <div col="3">
            <div className="card">
              <div className="card-column">
                <div className="card-column-icon">
                  <img src={image} alt="" />
                </div>
                <div className="card-column-title">
                  <h1 fs="24">Pure Android No clutter</h1>
                </div>
                <div className="card-column-desc">
                  No manufacturer apps to slow your device Optimized for speed
                  and performance.
                </div>
              </div>
            </div>
          </div>
          <div col="3">
            <div className="card">
              <div className="card-column">
                <div className="card-column-icon">
                  <img src={image} alt="" />
                </div>
                <div className="card-column-title">
                  <h1 fs="24">Pure Android No clutter</h1>
                </div>
                <div className="card-column-desc">
                  No manufacturer apps to slow your device Optimized for speed
                  and performance.
                </div>
              </div>
            </div>
          </div>
          <div col="6" marginTop="30">
            <div className="card">
              <div className="card-column">
                <div className="card-column-icon">
                  <img src={image} alt="" />
                </div>
                <div className="card-column-title">
                  <h1 fs="24">Pure Android No clutter</h1>
                </div>
                <div className="card-column-desc">
                  No manufacturer apps to slow your device Optimized for speed
                  and performance.
                </div>
              </div>
            </div>
          </div>
          <div col="6" marginTop="30">
            <div className="card">
              <div className="card-column">
                <div className="card-column-icon">
                  <img src={image} alt="" />
                </div>
                <div className="card-column-title">
                  <h1 fs="24">Pure Android No clutter</h1>
                </div>
                <div className="card-column-desc">
                  No manufacturer apps to slow your device Optimized for speed
                  and performance.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
