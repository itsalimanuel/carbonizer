window.onload = function () {
  const accordion = document.getElementsByClassName("dropdown");

  for (let i = 0; i < accordion.length; i++) {
    accordion[i].addEventListener("click", function () {
      this.classList.toggle("show");
    });
  }
};
