import React, { Component } from "react";
import "./index.scss";
import "./dropdown";
class Dropdown extends Component {
  render() {
    return (
      <div id="dropdown">
        <h1>Dropdown</h1>
        <div className="dropdown">
          <div className="dropdown-trigger">
            <button className="dropdown-button" id="dropTrigger">
              <span>Click me</span>
            </button>
          </div>
          <div className="dropdown-menu" id="dropList" role="menu">
            <div className="dropdown-item">dropdown-item 1</div>
            <div className="dropdown-item">dropdown-item 2</div>
            <div className="dropdown-item">dropdown-item 3</div>
          </div>
        </div>

        <hr />
        <h1>dropdrop hover style</h1>

        <ul className="dropdown dropdown-list">
          <a className="dropdown-action blueviolet" href="#">
            Carbon1
          </a>

          <ul className="dropdown-links clicksItems">
            <a href="#">Themes</a>

            <a href="#">Figma</a>

            <a href="#">XD</a>
          </ul>
        </ul>
      </div>
    );
  }
}

export default Dropdown;
