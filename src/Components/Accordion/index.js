import React, { Component } from "react";
import "./index.scss";
import "./Accordion";
class Accordion extends Component {
  render() {
    return (
      <div id="">
        <h1>Accordion</h1>
        <hr />
        <h2>accordion example</h2>
        <div className="container">
          <div className="accordion">
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button
                  accordion-button="1"
                  aria-expanded="false"
                  className="accordion-button"
                >
                  open
                </button>
              </h2>
              <div className="accordion-collapse collapse">
                <div id="1" className="accordion-body">
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Error voluptas cum beatae repellendus quidem, similique, autem
                  unde blanditiis impedit vel nemo voluptate cupiditate quasi
                  quas laboriosam minima dicta non dolor!
                </div>
              </div>
            </div>
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className="accordion-button">open1</button>
              </h2>
              <div className="accordion-collapse collapse">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Error voluptas cum beatae repellendus quidem, similique, autem
                  unde blanditiis impedit vel nemo voluptate cupiditate quasi
                  quas laboriosam minima dicta non dolor!
                </div>
              </div>
            </div>
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button className="accordion-button">open2</button>
              </h2>
              <div className="accordion-collapse collapse">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Error voluptas cum beatae repellendus quidem, similique, autem
                  unde blanditiis impedit vel nemo voluptate cupiditate quasi
                  quas laboriosam minima dicta non dolor!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Accordion;
