import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ColumnFlex from "./Columns/flex/index";
import ColumnGrid from "./Columns/grid/index";
import Welcome from "./welcome/index";
import Button from "./Elements/Button/index";
import Typography from "./Helpers/Typography/index";
import Card from "./Components/Card/index";
import Dropdown from "./Components/Dropdown";
import Colors from "./Helpers/colors";
import LinksPage from "./Elements/Links/index";
import Icons from "./Helpers/Icons";
import InputPage from "./Form/Input";
import SelectInputPage from "./Form/Select";
import BrakePoint from "./Helpers/Brakepoint";
import InputFile from "./Form/File";
import Container from "./Columns/Container";
import Display from "./Columns/display";
import FloatPage from "./Columns/float/index";
import Pagination from "./Components/pagination";
import Accordion from "./Components/Accordion";
function App() {
  return (
    <Router>
      <div className="App">
        <div id="carbonizer">
          <div className="carbonizer-list">
            <h1>Carbonizer</h1>
            <ul>
              <span>Layout</span>
              <li>
                <Link to="flex">Flex</Link>
              </li>
              <li>
                <Link to="grid">Grid</Link>
              </li>
              <li>
                <Link to="container">Container</Link>
              </li>
              <li>
                <Link to="display">Display</Link>
              </li>
              <li>
                <Link to="float">Float</Link>
              </li>
              <span>Components</span>
              <li>
                <Link to="/card">Card</Link>
              </li>
              <li>
                <Link to="accordion">Accordion</Link>
              </li>
              <li>
                <Link to="/dropdown">Dropdown</Link>
              </li>
              <span>Elements</span>
              <li>
                <Link to="/button">Button</Link>
              </li>
              <li>
                <Link to="/pagination">Pagination</Link>
              </li>
              <li>
                <Link to="/links">Links</Link>
              </li>
              <span>Form</span>
              <li>
                <Link to="input">Input</Link>
              </li>
              <li>
                <Link to="file">file</Link>
              </li>
              <li>
                <Link to="/select">Select</Link>
              </li>
              <span>Helpers</span>
              <li>
                <Link to="/brakepoint">Brakepoint</Link>
              </li>
              <li>
                <Link to="/typography">Typography</Link>
              </li>
              <li>
                <Link to="/colors">Colors</Link>
              </li>
              <li>
                <Link to="/icons">Icons</Link>
              </li>
              <span>Layout</span>
              <li>
                <Link to="">Layout</Link>
              </li>
            </ul>
          </div>
          <div className="carbonizer-main">
            <Switch>
              <Route exact path="/">
                <Welcome />
              </Route>
              <Route path="/flex">
                <ColumnFlex />
              </Route>
              <Route path="/grid">
                <ColumnGrid />
              </Route>
              <Route path="/accordion">
                <Accordion />
              </Route>
              <Route path="/card">
                <Card />
              </Route>
              <Route path="/container">
                <Container />
              </Route>
              <Route path="/display">
                <Display />
              </Route>
              <Route path="/float">
                <FloatPage />
              </Route>
              <Route path="/pagination">
                <Pagination />
              </Route>
              <Route path="/brakepoint">
                <BrakePoint />
              </Route>
              <Route path="/typography">
                <Typography />
              </Route>
              <Route path="/colors">
                <Colors />
              </Route>
              <Route path="/button">
                <Button />
              </Route>
              <Route path="/links">
                <LinksPage />
              </Route>
              <Route path="/select">
                <SelectInputPage />
              </Route>
              <Route path="/icons">
                <Icons />
              </Route>
              <Route path="/input">
                <InputPage />
              </Route>
              <Route path="/file">
                <InputFile />
              </Route>
              <Route path="/dropdown">
                <Dropdown />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
