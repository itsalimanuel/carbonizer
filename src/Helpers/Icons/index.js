import React, { Component } from "react";
import "./index.scss";

class Icons extends Component {
  render() {
    return (
      <div className="">
        <h1>Icons List</h1>
        <hr />
        <p> icon-carbon-copy</p>
        <i className="icon-carbon-copy"></i>
        <p> icon-carbon-caret-down</p>
        <i className="icon-carbon-caret-down"></i>
        <p>icon-carbon-arrow-left</p>
        <i className="icon-carbon-arrow-left"></i>
        <p>icon-carbon-area-chart</p>
        <i className="icon-carbon-area-chart"></i>
      </div>
    );
  }
}

export default Icons;
