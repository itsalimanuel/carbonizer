import React, { Component } from "react";
import "./index.scss";

class Colors extends Component {
  render() {
    return (
      <div className="">
        <h1>Colors system</h1>
        <h3 className="" color="brand">
          #2DE0DD $brand or we ca n use atr [color="brand"]
        </h3>
        <h3 className="" color="brand2">
          #40E08C $brand2 or we ca n use atr [color="brand2"]
        </h3>
        <h3 className="" color="secondary">
          #172DF7 $secondary or we ca n use atr [color="secondary"]
        </h3>
        <h3 className="" color="secondary2">
          #8A2BE2 $secondary2 or we ca n use atr [color="secondary2"]
        </h3>
      </div>
    );
  }
}

export default Colors;
