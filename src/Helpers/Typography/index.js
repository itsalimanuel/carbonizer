import React, { Component } from "react";
import "./index.scss";

class Typography extends Component {
  render() {
    return (
      <div className="">
        <h1>Typography</h1>
        <hr />
        <h1 className="h1" header="title">
          Header Title
        </h1>
        <h2 header="title">Header Title 2</h2>
        <h3 header="title">Header Title 3</h3>
        <h4 header="title">Header Title 4</h4>
        <h5 header="title">Header Title 5</h5>
        <h6 header="title">Header Title 6</h6>

        <hr />
        <h1 className="h1">paragraph </h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Non ea ad
          similique enim ipsa, voluptas perferendis voluptate numquam ipsam
          nihil modi, qui voluptates dolores magnam quasi expedita pariatur!
          Cum, laudantium.
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam
          asperiores cumque omnis sint odit commodi nihil est,{" "}
          <span color="red">
            SPAN PART earum, vitae repellendus officiis, quis laudantium
          </span>{" "}
          fuga deserunt necessitatibus fugiat eaque aliquid? Eum.
        </p>
      </div>
    );
  }
}

export default Typography;
