import React, { Component } from "react";
import "./index.scss";

class Display extends Component {
  render() {
    return (
      <div id="display">
        <h1>Display</h1>
        <hr />
        <ul>
          <li>none : .display-n</li>
          <li>inline : .display-i</li>
          <li>inline-block : .display-ib</li>
          <li>block : .display-b</li>
          <li>grid : .display-g</li>
          <li>table : .display-t</li>
          <li>table-cell : .display-tc</li>
          <li>table-row : .display-tr</li>
          <li>flex : .display-f</li>
          <li>inline-flex : .display-if</li>
        </ul>
        <hr />

        <div>
          .display-f - .md-fisplay-f - .sd-display-f - .li-display-f -
          .si-display-f - .mobile-display-f
        </div>
      </div>
    );
  }
}

export default Display;
