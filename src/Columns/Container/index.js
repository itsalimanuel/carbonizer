import React, { Component } from "react";
import "./index.scss";

class Container extends Component {
  render() {
    return (
      <div className="">
        <h1>Container</h1>
        <hr />
        <div className="container">Carbon Container</div>
      </div>
    );
  }
}
export default Container;
