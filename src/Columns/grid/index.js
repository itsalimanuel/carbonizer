import React, { Component } from "react";
import "./index.scss";
import "./grid";

class ColumnGrid extends Component {
  render() {
    return (
      <div id="grid">
        <h1>Grid Page</h1>
        <hr />
        <div className="container">
          <div className="row">
            <div className="col-cb">
              <h1>.col-cb</h1>
            </div>
            <div className="col-cb">
              <h1>.col-cb</h1>
            </div>
            <div className="col-cb">
              <h1>.col-cb</h1>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ColumnGrid;
