import React, { Component } from "react";
import "./index.scss";
import "./flex";

class ColumnFlex extends Component {
  render() {
    return (
      <div id="flex">
        <h1>Flex Page</h1>
        <p>
          You can add unlimte number of <strong>columns</strong>{" "}
          <code>.columns</code>
        </p>
        <hr />
        <div className="columns">
          <div className="column ">
            <p>column</p>
          </div>
          <div className="column">
            <p>column</p>
          </div>
          <div className="column">
            <p>column</p>
          </div>
          <div className="column">
            <p>column</p>
          </div>
        </div>
        <hr />
        <p>
          You can add unlimte number of <strong>columns</strong>{" "}
          <code>.flex</code>
          <code>col="3" xl-col="6" md-col="12" sm-col="6"</code>
        </p>
        <div className="flex">
          <div className="red blue" xl-col="6" col="3" md-col="12" sm-col="6">
            <p>column</p>
          </div>
          <div className="red" col="3" xl-col="6" md-col="12" sm-col="6">
            <p>column</p>
          </div>
          <div className="red" col="3" xl-col="6" md-col="12" sm-col="6">
            <p>column</p>
          </div>
          <div className="red" col="3" xl-col="6" md-col="12" sm-col="6">
            <p>column</p>
          </div>
        </div>
      </div>
    );
  }
}

export default ColumnFlex;
