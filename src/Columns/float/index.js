import React, { Component } from "react";
import "./index.scss";

class FloatPage extends Component {
  render() {
    return (
      <div id="float">
        <h1>Float</h1>
        <hr />
        <h2>Large Desktop</h2>
        <h3>.float-start - .float-end - .float-none</h3>
        <h2>M Desktop</h2>
        <h3>.md-float-start - .md-float-end - .md-float-none</h3>
        <h2>S Desktop</h2>
        <h3>.sd-float-start - .sd-float-end - .sd-float-none</h3>
        <h2>L Ipad</h2>
        <h3>.li-float-start - .li-float-end - .li-float-none</h3>
        <h2>S Ipad</h2>
        <h3>.si-float-start - .si-float-end - .si-float-none</h3>
        <h2>Mobile</h2>
        <h3>.mobile-float-start - .mobile-float-end - .mobile-float-none</h3>
        <hr />
        <div className="float-start">Text here</div>
        <div className="float-end">Text here</div>
        <br />
        <div className="float-none">Text here</div>
      </div>
    );
  }
}
export default FloatPage;
