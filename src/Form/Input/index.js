import React, { Component } from "react";
import "./index.scss";
import "./input";

class InputPage extends Component {
  render() {
    return (
      <div className="">
        <h1>Input Page</h1>
        <form action="">
          <label htmlFor="">Label text</label>
          <input
            required
            type="text"
            maxLength="40"
            placeholder="Your name max Lenght 40 more ts"
            fs="16"
          />
          <hr />
          <label htmlFor="">Label email</label>
          <input
            required
            id="email"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
            type="email"
            title="example@carbonmobile.com"
            placeholder="Your email"
            fs="16"
          />
          <hr />
          <label htmlFor="">Label tel</label>
          <input
            required
            id="tel"
            pattern="[\+()]*(?:\d[\s\-\.()xX]*){10,14}"
            type="tel"
            title="+7 (919) 766-91-80"
            placeholder="Your number"
            fs="16"
          />
          <hr />
          <label htmlFor="">Label textarea</label>
          <textarea
            required
            name=""
            maxLength="200"
            id=""
            cols="30"
            placeholder="your message"
            rows="10"
          ></textarea>
          <hr />
          <button className="btn" type="submit">
            send
          </button>
          <hr />
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
          </ul>
        </form>
      </div>
    );
  }
}

export default InputPage;
