import React, { Component } from "react";
import "./index.scss";

class SelectInputPage extends Component {
  render() {
    return (
      <div className="">
        <h1>Select Page</h1>

        <hr />
        <div className="select">
          <select fs="22" name="" id="">
            <option value="carbon">carbon</option>
            <option value="carbon1">carbon1</option>
            <option value="carbon2">carbon2</option>
            <option value="carbon3">carbon3</option>
          </select>
        </div>
      </div>
    );
  }
}

export default SelectInputPage;
