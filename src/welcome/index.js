import React, { Component } from "react";
import "./index.scss";
class Welcome extends Component {
  render() {
    return (
      <div className="">
        <h1>Welcome page</h1>
        <hr />
        <h3>Font weight in our site</h3>
        <hr />
        <div fw="100">Helvetica Neue - Thin fw="100"</div>
        <hr />
        <div fw="200">Helvetica Neue - light fw="200"</div>
        <hr />
        <div fw="300">Helvetica Neue - Medium fw="300"</div>
        <hr />
        <div fw="400">Helvetica Neue - Black fw="400"</div>
      </div>
    );
  }
}
export default Welcome;
